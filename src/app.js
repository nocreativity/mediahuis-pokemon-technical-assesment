import './app.scss';

import React, {useEffect} from 'react';
import {useQuery} from 'graphql-hooks'

import ALL_POKEMONS_QUERY from './graphql/all-pokemons';

import {usePokemons} from './contexts/pokemons';

import Logo from './components/logo';
import Container from './components/container';
import Configurator from './components/configurator';
import DreamTeam from './components/dream-team';

export default function App() {
    const {
        updateAllPokemons,
        team
    } = usePokemons();

    const { error, data } = useQuery(ALL_POKEMONS_QUERY, {
        variables: {
            limit: 151 // load all of them
        }
    });

    useEffect(() => {
        if (data && data.Pokemons) {
            updateAllPokemons(data.Pokemons);
        }
    }, [data, updateAllPokemons]);

    if (error) {
        console.log('error:', error);
        return 'Something Bad Happened'
    }

    return (
        <Container>
            <Logo />
            <Configurator />
            <DreamTeam team={team}/>
        </Container>
    );
}
