import './selected-moves.scss';

import React from 'react';

function SelectedMoves(props) {
    const createMovesList = () => {
        const elements = [];

        for(let i = 0; i < 4; i++) {
            const selectedItem = props.selectedMoves[i];

            if (selectedItem) {
                elements.push((<li key={'item-' + i} className="move-item">
                    <span className="move-item-label">{selectedItem.type}</span>
                    <span className="move-item-value">{selectedItem.name}</span>
                </li>));
            } else {
                elements.push((<li key={'item-' + i} className="move-item empty">
                    <span className="move-item-value">No move selected</span>
                </li>));
            }
        }

        return elements;
    };

    return (
        <div className="selected-moves">
            <h2>Selected moves</h2>
            <div>
                <ul className="moves">
                    {createMovesList()}
                </ul>
            </div>
        </div>
    );
}

export default SelectedMoves;