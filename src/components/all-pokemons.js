import './all-pokemons.scss';

import React, {useState} from 'react';
import {usePokemons} from '../contexts/pokemons';

function AllPokemons(props) {
    const {allPokemons} = usePokemons();
    const [currentFilterValue, setFilteredValue] = useState('');

    const pokemonsList = allPokemons
        .filter((pokemon) => pokemon.name.indexOf(currentFilterValue) !== -1)
        .map((pokemon) => (
            <li key={pokemon.name} onClick={() => props.onPokemonSelected(pokemon)} className="pokemon">{pokemon.name}</li>
        ));

    return (
        <div className="all-pokemons">
            <h1>Select a pokemon</h1>
            <input type="input" onChange={(event) => setFilteredValue(event.target.value)}/>
            <ul className="pokemon-list">
                {pokemonsList}
            </ul>
        </div>
    );
}

export default AllPokemons;