import './available-moves.scss';
import React, {useState} from 'react';
import {useQuery} from "graphql-hooks";
import SINGLE_POKEMON_QUERY from "../graphql/single-pokemon";
import {usePokemons} from "../contexts/pokemons";


function AvailableMoves(props) {
    const [selectedTab, setSelectedTab] = useState(null);
    const {getAvailableMoves} = usePokemons();

    const moveSelectedHandler = (move) => {
        props.onMoveSelected(move);
    };

    const processMoves = (movesList) => {
        return movesList
            .sort((moveA, moveB) => {
                if (moveA.type > moveB.type) {
                    return 1;
                } else if (moveA.type < moveB.type) {
                    return -1
                } else {
                    return 0;
                }
            })
            .map((move) => {
                return (<li key={move.name} onClick={() => moveSelectedHandler(move)} className="available-move-item">{move.name} ({move.type})</li>)
            });
    };

    const {loading, data} = useQuery(SINGLE_POKEMON_QUERY, {
        variables: {
            name: props.pokemon.name
        }
    });

    const pokemon = data && data.Pokemon
        ? data.Pokemon
        : null;

    const availableMoves = pokemon
        ? getAvailableMoves(pokemon)
        : [];

    if (availableMoves.length === 0) return null;

    const tabToShow = selectedTab
        ? selectedTab
        : availableMoves[0].learnMethod;

    const selectedGroup = availableMoves.find((group) => group.learnMethod === tabToShow);

    const tabHeaders = availableMoves.map((group) => {
        const cssClasses = group.learnMethod === tabToShow
            ? 'tab-header is-active'
            : 'tab-header';

        return (
            <div
                key={group.learnMethod}
                className={cssClasses}
                onClick={() => setSelectedTab(group.learnMethod)}
            >{group.learnMethod}</div>
        )
    });

    return (
        <div className="available-moves">
            <div className="tabs">
                {tabHeaders}
            </div>
            {selectedGroup && <ul className="available-moves-list">
                {processMoves(selectedGroup.moves)}
            </ul>}
        </div>
    )
}

export default AvailableMoves;