import './dream-team.scss';

import React from 'react';

export default function DreamTeam(props) {
    const processMoves = (moves) => {
        return moves.map((move) => (
            <li key={move.name} className="card-moves-item">{move.name}</li>
        ));
    };

    const getDecorationCssClass = (types) => {
        if (types.length === 0) {
            return 'card';
        }

        const type = types[0];

        if (!type) {
            return 'card';
        }

        return 'card type-' + type.name;
    }

    const createTeamList = () => {
        const elements = [];

        for(let i = 0; i < 6; i++) {
            const selectedItem = props.team[i];

            if (selectedItem) {
                elements.push((<li key={selectedItem.pokemon.name} className={getDecorationCssClass(selectedItem.pokemon.types)}>
                    <img className="card-image" src={selectedItem.pokemon.image} alt={'Image of ' + selectedItem.pokemon.name}/>
                    <h2>{selectedItem.pokemon.name}</h2>
                    <ul className="card-moves">
                        {processMoves(selectedItem.moves)}
                    </ul>
                </li>));
            } else {
                elements.push((<li key={'item-' + i} className="card empty">Empty slot</li>));
            }
        }

        return elements;
    };

      return (
        <section className="dream-team">
            <h1>Selected squad</h1>
            <ul className="cards">
                {createTeamList()}
            </ul>
        </section>
    );
}
