import './logo.css';

export default function Logo(props) {
  return (
      <div className="logo-container">
          <img
              {...props}
              className="logo"
              alt="Pokémon"
              src={process.env.PUBLIC_URL + '/logo.png'}
          />
      </div>
  );
}
