import './container.css';

export default function Container(props) {
  return <div {...props} className="container" />;
}
