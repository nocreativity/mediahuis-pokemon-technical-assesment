import './configurator.css';

import React from 'react';
import AllPokemons from "./all-pokemons";
import PokemonInfo from "./pokemon-info";
import PokemonStats from "./pokemon-stats";
import AvailableMoves from "./available-moves";
import {usePokemons} from "../contexts/pokemons";
import SelectedMoves from "./selected-moves";

export default function Configurator() {
    const {
        selectedPokemon,
        updateSelectedPokemon,
        selectedMoves,
        updateSelectedMoves,
        updateCurrentPokemonInfo,
        currentPokemonInfo,
        team,
        updateTeam,
        allPokemons,
        updateAllPokemons
    } = usePokemons();

    const handleSelectedPokemonChange = (newSelection) => {
        updateSelectedPokemon(newSelection);
        updateSelectedMoves([]);
    };

    const canStillAddNewMembersToTeam = team.length !== 6;
    const canAddMemberToTeamNow = selectedMoves.length !== 0;

    const handleMoveSelected = (newMove) => {
        const moveWasAlreadySelected = selectedMoves.indexOf(newMove) !== -1;

        if (moveWasAlreadySelected) {
            updateSelectedMoves(selectedMoves.filter((move) => move.name !== newMove.name));
        } else {
            if (selectedMoves.length !== 4) {
                updateSelectedMoves([...selectedMoves, newMove]);
            }
        }
    };

    const handleAddPokemonToTeam = () => {
        if (team.length !== 6) {
            const newMember = {
                pokemon: currentPokemonInfo,
                moves: selectedMoves
            };

            updateTeam([... team, newMember]);
            updateAllPokemons(allPokemons.filter((pokemon) => pokemon.name !== currentPokemonInfo.name))
            updateSelectedPokemon(null);
            updateCurrentPokemonInfo(null);
            updateSelectedMoves([]);
        }
    };

    return (
        <section className="configurator">
            <div className="panel pokemon-selector">
                <AllPokemons onPokemonSelected={handleSelectedPokemonChange}/>
            </div>
            <div className="panel pokemon-info">
                {selectedPokemon && <PokemonInfo pokemon={selectedPokemon}/>}
                {currentPokemonInfo && canStillAddNewMembersToTeam && <button disabled={!canAddMemberToTeamNow} className="button" onClick={() => handleAddPokemonToTeam()}>Add to team</button>}
            </div>
            <div className="panel pokemon-stats">
                {selectedPokemon && <PokemonStats pokemon={selectedPokemon}/>}
                {selectedMoves && <SelectedMoves selectedMoves={selectedMoves}/>}
            </div>
            <div className="panel pokemon-moves">
                {selectedPokemon && <AvailableMoves pokemon={selectedPokemon} onMoveSelected={handleMoveSelected}/>}
            </div>
        </section>
    );
}
