import './pokemon-stats.scss';

import React from 'react';
import {useQuery} from "graphql-hooks";
import SINGLE_POKEMON_QUERY from "../graphql/single-pokemon";

function PokemonStats(props) {
    const processStat = (stat) => {
        return (<li key={stat.name} className="stats-item">
            <span className="stats-item-label">{stat.name}:</span>
            <span className="stats-item-value">{stat.value}</span>
        </li>);
    }

    const {loading, data} = useQuery(SINGLE_POKEMON_QUERY, {
        variables: {
            name: props.pokemon.name
        }
    });

    const pokemon = data && data.Pokemon
        ? data.Pokemon
        : null;

    const statsList = pokemon
        ? pokemon.stats.map((stat) => processStat(stat))
        : null;

    if (!statsList) return null;

    return (
        <div className="pokemon-stats">
            <h2>Stats</h2>
            <div>
                <ul className="stats-overview">
                    {statsList}
                </ul>
            </div>
        </div>
    );
}

export default PokemonStats;