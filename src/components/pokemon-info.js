import './pokemon-info.css';

import React from 'react';
import {useQuery} from "graphql-hooks";
import SINGLE_POKEMON_QUERY from "../graphql/single-pokemon";
import {usePokemons} from "../contexts/pokemons";

function PokemonInfo(props) {
    const {
        updateCurrentPokemonInfo,
        currentPokemonInfo
    } = usePokemons();

    const {loading, data} = useQuery(SINGLE_POKEMON_QUERY, {
        variables: {
            name: props.pokemon.name
        }
    });

    const pokemon = data && data.Pokemon
        ? data.Pokemon
        : null;

    console.log('updating currentPokemonInfo', currentPokemonInfo, pokemon);
    updateCurrentPokemonInfo(pokemon);

    return (
        <React.Fragment>
            {loading && <div>Requesting data from Pokedex...</div>}
            {pokemon && <div>
                <img src={pokemon.image} alt={'Image of ' + pokemon.name}/>
                <h1 className="pokemon-name">{pokemon.name}</h1>
            </div>}
        </React.Fragment>
    );
}

export default PokemonInfo;