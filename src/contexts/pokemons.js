import React, {useContext, useState} from 'react';

export const PokemonsContext = React.createContext();

export function usePokemons() {
    return useContext(PokemonsContext);
}

export function PokemonsProvider({children}) {
    const [allPokemons, updateAllPokemons] = useState([]);
    const [selectedPokemon, updateSelectedPokemon] = useState(null);
    const [currentPokemonInfo, updateCurrentPokemonInfo] = useState(null);
    const [selectedMoves, updateSelectedMoves] = useState([]);
    const [team, updateTeam] = useState([]);

    const api = {
        allPokemons,
        updateAllPokemons,
        getAvailableMoves,
        selectedPokemon,
        updateSelectedPokemon,
        selectedMoves,
        updateSelectedMoves,
        team,
        updateTeam,
        currentPokemonInfo,
        updateCurrentPokemonInfo
    };

    return (<PokemonsContext.Provider value={api}>
        {children}
    </PokemonsContext.Provider>);
}

function getAvailableMoves(pokemon) {
    const allMovesByLearningMethod = {};

    pokemon.moves.forEach((move) => {
        const learnMethod = move.learnMethod;

        if (!allMovesByLearningMethod[learnMethod]) {
            allMovesByLearningMethod[learnMethod] = [];
        }

        const learnMethodList = allMovesByLearningMethod[learnMethod];
        learnMethodList.push(move);
    });

    return Object
        .keys(allMovesByLearningMethod)
        .sort()
        .map((learnMethod) => ({
            learnMethod,
            moves: allMovesByLearningMethod[learnMethod]
        }));
};