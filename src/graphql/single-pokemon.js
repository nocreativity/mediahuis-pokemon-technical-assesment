const SINGLE_POKEMON_QUERY = `
  query SinglePokemons($name: String!) {
        Pokemon(name: $name) {
          id
          name
          image
          abilities {
            name
          }
          stats {
            name
            value
          }
          types {
            name
          }
          moves {
            name
            type
            learnMethod
          }
        }
    }`;

export default SINGLE_POKEMON_QUERY;