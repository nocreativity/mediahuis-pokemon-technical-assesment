const ALL_POKEMONS_QUERY = `
query AllPokemons($limit: Int) {
  Pokemons(first: $limit) {
    id
    name
  }
}`;

export default ALL_POKEMONS_QUERY;