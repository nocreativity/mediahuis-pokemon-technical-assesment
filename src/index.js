import React from 'react';
import ReactDOM from 'react-dom';
import { ClientContext, GraphQLClient } from 'graphql-hooks';

import App from './app';
import { PokemonsProvider } from './contexts/pokemons';

const client = new GraphQLClient({
    url: process.env.REACT_APP_POKE_ENDPOINT,
});

ReactDOM.render(
    <React.StrictMode>
        <ClientContext.Provider value={client}>
            <PokemonsProvider>
                <App />
            </PokemonsProvider>
        </ClientContext.Provider>
    </React.StrictMode>,
    document.getElementById('root')
);